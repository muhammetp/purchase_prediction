## Project Description

In e-commerce, user's propensity towards product categories or brands gives a valuable insight for giving users a better experience and for making critical decisions more accurately.

In this project, we expect you to build a model(s) that can predict the probability of a customer to order a product from the given **currentbugroupname** in the next 3 days (test period). For building your model, you can use various customer data given, such as past orders, search terms, products visited, user favorites, products added to the basket, product details and user demographics.

## Data Description
### df_basket: Users' add to basket data
- **userid:** Unique ID of the user who has added the product to his/her basket. 
- **contentid:** Unique ID of the product which has been added to the basket by the user.
- **partition_date:** Date and hour of the "add to basket" event.
- **addtobasket_count:** How many times this event occured during this partition_date.

### df_fav: Users' add to favorite data
- **userid:** Unique ID of the user who has added the product to his/her favorites.
- **contentid:** Unique ID of the product which has been added to the favorites by the user.
- **partition_date:** Date and hour of the "add to favorites" event.
- **fav_count:** How many times this event occured during this partition_date.


### df_visit: Users' product visit data 
- **userid:** Unique ID of the user who has visited the product.
- **contentid:** Unique ID of the product which has been visited by the user.
- **partition_date:** Date and hour of the "product visit" event.
- **productdetailcount:** How many times this event occured during this partition_date.

### df_trx: Users' product purchase data 
- **userid:** Unique ID of the user who has purchased the product.
- **contentid:** Unique ID of the product which has been purchased by the user.
- **orderdate:** Date and hour of the "product purchase" event.
- **quantity:** How many units of this product is purchased during this orderdate.
- **price:** Price of the purchased product.

### df_search_term: Users' term searching data 
- **userid:** Unique ID of the user who has searched this seearch term
- **partition_date:** Date and hour of the "search" event.
- **search_term:** The term searched by the user.

### df_product: Products' information data
- **contentid:** Unique ID given to the product.
- **currentbugroupname:** Business Group Name (can be thought of as a broader category) of the product.
- **brandid:** Unique ID given to the brand of the product.
- **gender:** Target gender group of the product.
- **age:** Target age group of the product.
- **title:** Title of the product.
- **categoryname:** Category name of the product.

### df_demo: Users' demographics data
- **userid:** Unique ID given to the user.
- **gender:** Specified gender of the user.
- **age:** Specified age of the user.
- **tenure:** Trendyol membership age of the user.

### df_target_train: Purchase data for **training** users in the next 3 days
- **userid:** Unique ID given to the user.
- **currentbugroupname:** Business Group Name the user has made a purchase from.

### df_test: Purchase prediction data for **test** users in the next 3 days
- **userid:** Unique ID given to the user.
- **currentbugroupname:** Business Group Name to calculate the user's purchase probability.
- **probability:** Users purchase probability. (between 0 and 1)
- **target:** Final verdict for the user's purchase decision (0 or 1)

## Deliverables
- A presentation about the modelling process that is most convenient for you (pdf, pptx, xlsx, ipynb etc. are all welcomed).
- Your code (Clean and commented code is a plus).
- You model's predictions for `df_test` dataset.

## Notes
- Keep in mind that a user can make a purchase from multiple **currentbugroupname**'s during the testing period.
- `df_basket`, `df_fav`, `df_visit`, `df_trx`, `df_search_term` & `df_demo` consist of the data of users in both `df_target_train` and `df_test`. 
